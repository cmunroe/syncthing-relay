# SyncThing Relay Dockerized

## Goals

When I was looking for options to run a SyncThing Relay on docker I came up with many options, none of which I felt covered what I wanted it to do. The official SyncThing Relay docker image supports no variables in its installation. Others support half the options. Others don't even show how they are being built.

As such, I wanted a simple, clear, easy to understand, and small dockerized SyncThing Relay image.

## Variables

All Variables should match official documentation except the "-" characters are swapped for "_". This is because of ENV variable limitations. These can be easily set in your docker-compose.yaml or command line.

## Variable List
       ext_address=<address>
              An  optional  address  to advertising as being available on. Allows listening on an
              unprivileged port with port forwarding from e.g. 443, and be connected to  on  port
              443 (default unset).

       global_rate=<bytes/s>
              Global rate limit, in bytes/s. (default 2500000 (2.5MB/s)).

       keys=<dir>
              Directory where cert.pem and key.pem is stored (default “.”).

       listen=<listen addr>
              Protocol listen address (default “:22067”).

       message_timeout=<duration>
              Maximum amount of time we wait for relevant messages to arrive (default 1m0s).

       nat_lease=<duration>
              NAT lease length in minutes (default 60)

       nat_renewal=<duration>
              NAT renewal frequency in minutes (default 30)

       nat_timeout=<duration>
              NAT discovery timeout in seconds (default 10)

       network_timeout=<duration>
              Timeout  for  network  operations  between  the client and the relay. If no data is
              received between the client and the relay in this period of time, the connection is
              terminated.  Furthermore,  if  no data is sent between either clients being relayed
              within this period of time, the session is also terminated. (default 2m0s)

       per_session_rate=<bytes/s>
              Per session rate limit, in bytes/s (default 1000000 (1000kB/s)).

       ping_interval=<duration>
              How often pings are sent (default 1m0s).

       pools=<pool addresses>
              Comma  separated   list   of   relay   pool   addresses   to   join   (default   “‐
              http://relays.syncthing.net/endpoint”).  Blank  to  disable announcement to a pool,
              thereby remaining a private relay.

       protocol=<string>
              Protocol used for listening. ‘tcp’ for IPv4 and IPv6, ‘tcp4’ for IPv4,  ‘tcp6’  for
              IPv6 (default “tcp”).

       provided_by=<string>
              An optional description about who provides the relay (default "cmunroe/syncthingrelay").

       status_srv=<listen addr>
              Listen  address  for  status service (blank to disable) (default “:22070”).  Status
              service is used by the relay pool server UI for displaying stats (data  transfered,
              number of clients, etc.)
              
## Rates

| Mbps   |  Rate     |
| -----  | ----------|
|  10    | 1250000   |
|  15    | 1875000   |
|  30    | 3750000   |
| 100    | 12500000  |
| 150    | 18750000  |
| 300    | 37500000  |
| 1000   | 125000000 |


## docker-compose.yaml
```
version: '3'

services:
  relay:
    container_name: syncthing
    restart: always
    image: cmunroe/syncthingrelay
    environment:
      - global_rate=2500000
      - per_session_rate=1000000
    ports:
      - 22067:22067
      - 22070:22070
```

## docker cli
```
docker run -d --name syncthing --restart always -p 22067:22067 -p 22070:22070 -e "per_session_rate=1000000" -e "global_rate=2500000" cmunroe/syncthingrelay
```

## Statistics

If you are in the public pool you can see your server listed here.

https://relays.syncthing.net/

On restart of your docker instances, the stats are reset and start over.


## Bandwidth

I've set the image to by default use around 20 Mbps of bandwidth in and out. This will roughly mean 40 Mbps of total bandwidth in and out. If you are running this on a server or network with a limited amount of bandwidth, you may wish to adjust the `global_rate` env variable.

## Code

https://gitlab.com/cmunroe/syncthing-relay